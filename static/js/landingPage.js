
$(document).ready(function(){
    refresh();

    function refresh() {
        $.ajax({
            method: 'GET',
            url: '/laporan/getDataFull/',
            dataType: 'json',
            success: function(data) {
                console.log(data)
                $('.angka-statistik').text(data.length);
                setTimeout(refresh, 1000);
            }
        });
    }


});

function hide() {
    var x = document.getElementById("password");
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  }