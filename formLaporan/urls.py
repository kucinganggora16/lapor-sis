from django.urls import path
from formLaporan import views

app_name = 'formLaporan'

urlpatterns = [
    path('', views.buatLaporan, name='buatLaporan'),
]