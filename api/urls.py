from django.urls import path
from . import views

app_name = 'api'

urlpatterns = [
    path('', views.LaporanView.as_view(), name='LaporanView'),
    path('create/', views.LaporanCreateView.as_view(), name='LaporanView'),   
]