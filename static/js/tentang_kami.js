$(document).ready(function(){
    var offset=2000;

    $('#submit_btn').click(function(){
        // var key = $('#search').val();
        $(function(){
            $.ajax({        
                url: 'json/',
                success: function(result){
                    $('#tbody').empty();
                    // https://stackoverflow.com/questions/12694135/how-to-append-json-array-data-to-html-table-tbody
                    $.each(result.items, function(index, value){
                        console.log(value);
                        var row = '<tr>';
                        row+='<td>'+ value.nama + '>' +'</td>';
                        row+='<td>'+ value.question +'</td>';
                        row+='<td>Pending</td>';
                        row += '</tr>';
                        $('#tbody').append(row);
                    });     
                }
            });
        });
    });

    $("#sis").hover(function(){
        $("#sis").css({
            "color": "#f2858a"
        });
        $("#laporin").css({
            "color": "#b1cbd8"
        });
        }, function(){
        $("#sis").css({
            "color": "#b1cbd8"
        });
        $("#laporin").css({
            "color": "#f2858a"
        });
    });
    $("#laporin").hover(function(){
        $("#sis").css({
            "color": "#f2858a"
        });
        $("#laporin").css({
            "color": "#b1cbd8"
        });
        }, function(){
        $("#sis").css({
            "color": "#b1cbd8"
        });
        $("#laporin").css({
            "color": "#f2858a"
        });
    });
    
    var count = 0;
    $("#alur").click(function(){
        if(count==0){
            $(".pop_up").fadeIn();
            count = 1;
        }else{
            $(".pop_up").fadeOut();
            count = 0;
        }
        
    });

    $(window).scroll(function(){
        if($(this).scrollTop()>offset){
            $('.to-top').fadeIn();
        }else{
            $('.to-top').fadeOut();
        }

    });
});