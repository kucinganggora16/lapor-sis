from django import forms
from .models import Laporan


class BuatLaporan(forms.ModelForm):
    class Meta():
        model = Laporan
        fields = ['nama', 'kontak', 'laporan']
        widgets = {
            'nama' : forms.TextInput(
                attrs = {
                    'class' : 'form-control'
                }
            ),

            'kontak' : forms.TextInput(
                attrs = {
                    'class' : 'form-control'
                }
            ),

            'laporan' : forms.Textarea(
                attrs = {
                    'class' : 'form-control'
                }
            )
        }
        