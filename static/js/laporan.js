
const myUrl = "http://localhost:8000"

let dataSearch = []

$(document).ready(function(){
    $('#button-search').click(function(){
        $('#result').html('');
        $("#t_body").html("");
        for(var j = 0; j < dataSearch.length; j++){
            let dataNama2 = dataSearch[j].fields.nama;
            let dataStatus2 = dataSearch[j].fields.status;
            let dataLaporan2 = dataSearch[j].fields.laporan;

            switch(dataStatus2){
                case 1:
                    dataStatus2 = "Diproses"
                    break;
                case 2:
                    dataStatus2 = "Ditolak"
                    break;
                case 3:
                    dataStatus2 = "Selesai" 
            }

            $('#result').html("");
            $('#search').val("");
            $("#t_body").append(`<tr>
                <td class="text-center">${dataNama2}</td>
                <td class="text-center">Pencurian</td>
                <td class="text-center">${dataStatus2}</td>
                <td class="text-center"><p>
                    <button id="details_button" class="btn btn-light button-pink" type="button" data-toggle="collapse" data-target="#collapseExample${dataSearch[j].pk}" aria-expanded="false" aria-controls="collapseExample">
                        Details
                    </button>
                    </p>
                    <div class="collapse" id="collapseExample${dataSearch[j].pk}">
                    <div class="card card-body">
                        <div class="text-justify">${dataLaporan2}</div>
                    </div>
                    </div>
                </td>
             </tr>`);

        }

    });
    
    
    $('#search').on('propertychange input', function(){
        $('#result').html('');
        var searchField = $('#search').val();
        var expression = new RegExp(searchField, "i");
    
        $.ajax({
          method: 'GET',
          url: `/laporan/getData/`+searchField,
          dataType: 'json',
          success: function(data){
            dataSearch = data;
            console.log(data);
            $('#result').html("");
              for(var i = 0; i < data.length; i++){
                let dataNama = data[i].fields.nama;
                let dataStatus = data[i].fields.status;
                let dataLaporan = data[i].fields.laporan;

                switch(dataStatus){
                    case 1:
                        dataStatus = "Diproses"
                        break;
                    case 2:
                        dataStatus = "Ditolak"
                        break;
                    case 3:
                        dataStatus = "Selesai"       
                }

                $('#result').append(`<li class="list-group-item"  id="data-laporan-${data[i].pk}"><a href="#">${dataNama}</a>`);
                $(`#data-laporan-${data[i].pk}`).on({
                    click:() =>{
                        $('#result').html("");
                        $('#search').val("");
                        $("#t_body").html(`<tr>
                        <td class="text-center">${dataNama}</td>
                        <td class="text-center">Pencurian</td>
                        <td class="text-center">${dataStatus}</td>
                        <td class="text-center"><p>
                            <button id="details_button" class="btn btn-light button-pink" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                              Details
                            </button>
                          </p>
                          <div class="collapse" id="collapseExample">
                            <div class="card card-body">
                              <div class="text-justify">${dataLaporan}</div>
                            </div>
                          </div>
                        </td>
                    </tr>`);
                    }
                })
              }             
            }
        });
    });
});
