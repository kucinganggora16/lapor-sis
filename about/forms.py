from django import forms
from .models import FAQ
from django.forms import ModelForm

class FAQform(forms.ModelForm):

    class Meta:
        model = FAQ
        fields = ['name', 'email', 'question']
        labels = {'name': 'Nama', 'email': 'E-mail', 'question': 'Pertanyaan'}
        widgets={
            'name': forms.TextInput(attrs={'class': 'form-control', 'id':'nameformnya'}),
            'email': forms.EmailInput(attrs={'class': 'form-control', 'id':'emailformnya'}),
            'question': forms.Textarea(attrs={'class': 'form-control', 'rows':'4', 'id':'questionformnya'})
        }