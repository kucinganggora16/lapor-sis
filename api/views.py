from django.shortcuts import render
from .serializers import LaporanSerializers
from rest_framework import viewsets, generics
from formLaporan.models import Laporan
from django.http import JsonResponse

# Create your views here.
class LaporanView(generics.ListAPIView):
    serializer_class = LaporanSerializers
    queryset = Laporan.objects.all()

class LaporanCreateView(generics.ListCreateAPIView):
    serializer_class = LaporanSerializers
    queryset = Laporan.objects.all()