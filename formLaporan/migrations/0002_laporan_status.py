# Generated by Django 2.2.5 on 2019-12-06 03:30

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('laporan', '0001_initial'),
        ('formLaporan', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='laporan',
            name='status',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.DO_NOTHING, to='laporan.PilihStatus'),
        ),
    ]
