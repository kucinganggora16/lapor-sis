from django.urls import path
from . import views

app_name="about"

urlpatterns = [
    path('', views.tentang_kami, name="tentang-kami"),
    path('tambah-pertanyaan/', views.question, name='question'),
    path('json/', views.jsonnya, name='jsonnya'),
]
