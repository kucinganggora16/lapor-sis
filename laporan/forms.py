from django import forms
from formLaporan.models import Laporan
from django.forms import ModelForm

class FormPilihStatus(ModelForm):
    class Meta:
        model = Laporan
        fields = ['status']
        labels = {'status' : ''}
        widgets = {'status' : forms.Select(
            attrs={
                'class' : 'form-control',
                'placeholder' : "jeje"
                }
            )}