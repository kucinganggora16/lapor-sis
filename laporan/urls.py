from django.urls import path, include
from laporan import views

app_name = 'laporan'

urlpatterns = [
    path('lihatLaporan/', views.lihatLaporan, name='lihatLaporan'),
    path('ubahLaporan/', views.ubahLaporan, name='ubahLaporan'),
    path('ubahLaporan/<int:id>', views.ubahStatusLaporan, name='ubahStatusLaporan'),
    path('lihatUbahLaporan/', views.lihatUbahLaporan, name='lihatUbahLaporan'),
    path('getData/', views.getData, name='getData'),
    path('getDataFull/', views.getDataFull, name='getDataFull'),
    path('getData/<str:nama>', views.getDataNama, name='getDataNama'),
]