from django.urls import path, include
from landingPage import views

app_name = 'landingPage'

urlpatterns = [
    path('', views.index, name="beranda"),
    path('login/', views.login_index, name="login"),
    path('register/', views.register, name="register"),
    path('logout/', views.logout_view, name="logout"),
]