from django.shortcuts import render, redirect
from django.core import serializers
from django.http import HttpResponse

from formLaporan.models import Laporan
from formLaporan.forms import BuatLaporan
from .forms import FormPilihStatus
# Create your views here.

def lihatLaporan(request):
    respon = Laporan.objects.order_by('-timestamp')
    konten = {'title' : 'Lihat Status Laporan', 'respon' : respon}
    return render(request, 'laporan/lihat_laporan.html', konten)

def ubahLaporan(request):
    respon = Laporan.objects.order_by('-timestamp')
    form = FormPilihStatus()
    konten = {'title' : 'Ubah Status Laporan', 'respon' : respon, 'lapor' : form}
    return render(request, 'laporan/ubah_laporan.html', konten)

def lihatUbahLaporan(request):
    if request.user.is_authenticated:
        return redirect('/laporan/ubahLaporan')
    else:
        return redirect('/laporan/lihatLaporan')

def ubahStatusLaporan(request, id):
    ubahStatus = Laporan.objects.get(pk=id)
    respon = Laporan.objects.order_by('-timestamp')
    form = FormPilihStatus(request.POST or None, instance=ubahStatus)
    if form.is_valid():
        ubahStatus = form.save(commit=False)
        ubahStatus.save()
        konten = {'title' : 'Ubah Status Laporan', 'form' : form, 'respon' : respon, 'ubahStatus' : ubahStatus}
        return redirect('/laporan/ubahLaporan')
    else:
        konten = {'title' : 'Ubah Status Laporan', 'form' : form, 'respon' : respon, 'ubahStatus' : ubahStatus}
        return redirect('/laporan/ubahLaporan')

def getData(request):
    return HttpResponse("[]", content_type="text/json-comment-filtered")

def getDataNama(request, nama):
    dataNama = Laporan.objects.filter(nama__contains = nama)
    laporan_list = serializers.serialize('json', dataNama)
    return HttpResponse(laporan_list, content_type="text/json-comment-filtered")

def getDataFull(request):
    data_full = Laporan.objects.all()
    data_full_list = serializers.serialize('json', data_full)
    return HttpResponse(data_full_list, content_type="text/json-comment-filtered")

