from rest_framework import serializers
from formLaporan.models import Laporan

class LaporanSerializers(serializers.ModelSerializer):
    class Meta:
        model = Laporan
        fields = ['nama', 'kontak', 'laporan']
