from django.test import TestCase, Client
from django.http import HttpRequest
from django.urls import reverse, resolve
from django.contrib.auth.models import User

from laporan.views import lihatLaporan, ubahLaporan
from laporan.models import PilihStatus
from laporan.forms import FormPilihStatus

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import Select
import time

# Create your tests here.

class LaporanTestCase(TestCase):
    def test_lihatLaporan_url_exist(self):
        client = Client()
        response = client.get(reverse('laporan:lihatLaporan'))
        self.assertEqual(response.status_code, 200)

    def test_ubahLaporan_url_exist(self):
        client = Client()
        response = client.get(reverse('laporan:ubahLaporan'))
        self.assertEqual(response.status_code, 200)

    def test_lihatLaporan_using_right_template(self):
        response = self.client.get(reverse('laporan:lihatLaporan'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'laporan/lihat_laporan.html')

    def test_ubahLaporan_using_right_template(self):
        response = self.client.get(reverse('laporan:ubahLaporan'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'laporan/ubah_laporan.html')

    def test_lihatLaporan_using_right_html(self):
        response = self.client.get(reverse('laporan:lihatLaporan'))
        self.assertContains(response, '<div id="lihat_laporan">')

    def test_ubahLaporan_using_right_html(self):
        response = self.client.get(reverse('laporan:ubahLaporan'))
        self.assertContains(response, '<div id="ubah_laporan">')

    def test_ubahLaporan_can_create_new_status(self):
        PilihStatus.objects.create(status='Ditolak')
        PilihStatus_count = PilihStatus.objects.all().count()
        self.assertEqual(PilihStatus_count, 1)

    def test_ubahLaporan_empty_status_didnt_count(self):
        Client().post(reverse('laporan:ubahLaporan'), {'status': ''})
        PilihStatus_count = PilihStatus.objects.all().count()
        self.assertEqual(PilihStatus_count, 0)

    def test_lihatLaporan_page_using_right_function(self):
        found = resolve(reverse('laporan:lihatLaporan'))
        self.assertEqual(found.func, lihatLaporan)

    def test_ubahLaporan_page_using_right_function(self):
        found = resolve(reverse('laporan:ubahLaporan'))
        self.assertEqual(found.func, ubahLaporan)

    def test_database_status_as_str(self):
        #Creating a new activity
        new_activity = PilihStatus.objects.create(status="Diproses")
        self.assertEqual(str(new_activity),"Diproses")

    def test_ubahLaporan_can_update_status(self):
        obj = PilihStatus.objects.create(status="Diproses")
        PilihStatus.objects.filter(pk=obj.pk).update(status="Ditolak")
        obj.refresh_from_db()
        self.assertEqual(obj.status, "Ditolak")

    def test_FormPilihStatus_using_css_class(self):
        form = FormPilihStatus()
        self.assertIn('class="form-control"', form.as_p())

    # def test_is_admin(self):
    #     user = User.objects.create(username='testuser')
    #     user.set_password('12345')
    #     user.save()
    #     c = Client()
    #     logged_in = c.login(username='testuser', password='12345')
        
    #     response = Client().get('laporan/lihatUbahLaporan/')
    #     self.assertTemplateUsed(response, 'laporan/ubah_laporan.html')

    # def test_is_not_admin(self):
    #     response = Client().get('laporan/lihatUbahLaporan/')
    #     self.assertTemplateUsed(response, 'laporan/lihat_laporan.html')

    # def test_redirect(self):
    #     response = Client().get('laporan/lihatLaporan/', follow =True)
    #     self.assertEqual(response.status_code, 302)

    def test_getData_url_exist(self):
        client = Client()
        response = client.get(reverse('laporan:getData'))
        self.assertEqual(response.status_code, 200)

    # def test_getDataNama_url_exist(self):
    #     client = Client()
    #     response = client.get(reverse('laporan:getDataNama')+'a')
    #     self.assertEqual(response.status_code, 200)

    def test_getDataFull_url_exist(self):
        client = Client()
        response = client.get(reverse('laporan:getDataFull'))
        self.assertEqual(response.status_code, 200)

class FuncTestLaporan(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FuncTestLaporan, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FuncTestLaporan, self).tearDown()

    def test_ubah_status(self):
        self.selenium.get('http://lapor-sis2.herokuapp.com/laporan/ubahLaporan')
        time.sleep(3)

        drpStatus = Select(self.selenium.find_element_by_id('id_status'))
        drpStatus.select_by_visible_text("Diproses")
        time.sleep(3)


