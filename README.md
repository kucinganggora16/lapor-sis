**Nama:**
1. Andhika Yusup Maulana
2. Dinda Inas Putri
3. Jovi Handono Hutama
4. Zafira Binta Feliandra


**Link herokuapp:**
<br>
www.lapor-sis.herokuapp.com
    
**Cerita aplikasi serta manfaat:**
    <br>
    Namanya Rina. Dia hanyalah salah satu dari sekian banyak korban kekerasan seksual. Di usia belianya, dia sudah berkali-kali **dipaksa** melakukan hal-hal yang seharusnya tidak dilakukan oleh anak seusianya. Kalau tidak dituruti, si pelaku akan memberi ancaman yang membuatdia melakukan hal tersebut dan tidak bercerita. Kemana seharusnya Rina bercerita untuk melapangkan dadanya? Rina tidak berani bercerita pada temannya, dia takut berita ini akan tersebar sehingga dia akan diolok-olok oleh temannya. Ibunya? Rina takut kalau dia akan menyakiti ibunya. Warga? Bah, Kalau warga tau, dia mungkin akan dikawinkan dengan pelakunya dengan alasan menghindari zina.. Lalu kemana dia akan bercerita???
    <br>
    <br>
    Sudah ada banyak cerita dan berita tentang kasus seperti Rina. Kebanyakan akhir cerita dari kasus ini adalah korban kekerasan seksual menyimpan "kutukan" tersebut sampai akhir hayat hidupnya. Cerita lain jika dia memutuskan untuk bercerita, namun akhirnya tidak jauh dari damai atau dikawinkan dengan pelaku. Pihak-pihak yang membantu menangani pun tidak dapat menyelesaikan masalah ini dengan tepat. Kami rasa perlu ada pihak yang mengerti dengan masalah ini, seperti Komnas Perempuan atau LSM yang bergerak di bidang terkait. Oleh karena itu, **kami membuat website untuk menghubungkan orang-orang seperti Rina dengan pihak yang profesional dalam menangani kasus ini.**
    <br>
    <br>
    Website ini menawarkan para penyintas kekerasan seksual bantuan-bantuan yang layak: Bantuan Hukum, Konseling Trauma, Perlindungan HAM. Bantuan Hukum akan diberikan kepada para penyintas yang menginginkan keadilan terhadap pelaku. Selanjutnya, Kami juga peduli terhadap kesehatan mental mereka. Oleh karena itu kami juga akan hubungkan mereka dengan Psikolog secara gratis. Kami akan melindungi para korban ini dari gangguan-gangguan yang akan datang.



**Fitur Admin**
1.  admin bisa melihat respon jawaban dari para pengguna
2.  admin bisa meng-update status laporan pengguna (diproses, ditolak, atau sudah selesai)
3.  admin bisa mendaftarkan akunnya

**Fitur Pengguna**
1. pengguna bisa mensubmit form laporan agar dapat dihubungkan dengan pihak terkait
2. website akan langsung mengidentifikasi jenis laporan sehingga pengguna hanya dituntut untuk bercerita saja
3. pengguna bisa melihat status laporannya
4. pengguna bisa menyampaikan pertanyaan kepada Lapor-sis!
5. Teknologi 4.0 yang dimiliki website ini akan langsung mengidengtifikasi laporan menggunakan teknologi NLP

**Status Pipelines**
<br>
[![pipeline](https://gitlab.com/kucinganggora16/lapor-sis/badges/master/pipeline.svg)](lapor-sis.herokuapp.com)
[![coverage report](https://gitlab.com/kucinganggora16/lapor-sis/badges/master/coverage.svg)](https://gitlab.com/kucinganggora16/lapor-sis/commits/master)