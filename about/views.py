from django.shortcuts import render, redirect
from .forms import FAQform
from .models import FAQ
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.core.serializers.json import DjangoJSONEncoder
from django.http import HttpResponse

import json

def tentang_kami(request):
    form=FAQform()        
    database = FAQ.objects.all()
    data = {'form':form, 'database': database}
    return render(request, 'about/tentang_kami.html', data)

def question(request):
    if request.method == 'POST':
        form = FAQform(request.POST)
        if form.is_valid():
            fields=['name','email','question']
            form.save()
    return HttpResponseRedirect("/tentang-kami")

def jsonnya(request):
    # melihat punya thami,siti
    if(request.method=="GET"):
        allFAQ = FAQ.objects.all()
        namaFAQ = []
        questionFAQ = []
        for i in allFAQ:
            questionFAQ.append({
                'nama':i.name,
                'question':i.question,
            })
        result=json.dumps(questionFAQ, cls=DjangoJSONEncoder)
        return HttpResponse(content=result, content_type="application/json")
    return HttpResponse("[]")