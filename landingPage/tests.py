from django.test import TestCase, Client
from django.urls import resolve
from landingPage.views import index, login_index, register
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.contrib.auth.models import User
from django.contrib import auth


from django.contrib.staticfiles.testing import StaticLiveServerTestCase


# Create your tests here.
class landingPageExistTestcase(TestCase):
    def test_landing_page_url_exist(self):
        respose = Client().get("/")
        self.assertEqual(respose.status_code, 200)

    def test_landing_page_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'landingPage/index.html')

    def test_landing_page_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

class LoginExistTestcase(TestCase):
    def test_login_url_exist(self):
        respose = Client().get("/login/")
        self.assertEqual(respose.status_code, 200)

    def test_login_template(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'landingPage/login.html')

    def test_login_using_index_func(self):
        found = resolve('/login/')
        self.assertEqual(found.func, login_index)

class RegisterExistTestcase(TestCase):
    def test_register_url_exist(self):
        respose = Client().get("/register/")
        self.assertEqual(respose.status_code, 200)

    def test_register_template(self):
        response = Client().get('/register/')
        self.assertTemplateUsed(response, 'landingPage/register.html')

    def test_register_using_index_func(self):
        found = resolve('/register/')
        self.assertEqual(found.func, register)

class loginTestCase(TestCase):
    def test_user_can_login(self):
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()

        c = Client()
        logged_in = c.login(username='testuser', password='12345')
        self.assertTrue(logged_in)

    def test_user_can_login(self):
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()

        c = Client()
        logged_in = c.login(username='testuser', password='12345')
        self.assertTrue(logged_in)
        has_logout = c.logout()
        
        anoni = auth.get_user(c)
        self.assertFalse(anoni.is_authenticated)


    



class FuncTestLandingPage(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FuncTestLandingPage, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FuncTestLandingPage, self).tearDown()

    def test_can_open_web(self):
        self.selenium.get("http://127.0.0.1:8000/")

    def test_can_open_login(self):
        self.selenium.get("http://127.0.0.1:8000/login/")

    def test_can_open_register(self):
        self.selenium.get("http://127.0.0.1:8000/register/")