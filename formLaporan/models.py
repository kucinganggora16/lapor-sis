from django.db import models

# Create your models here.
class Laporan(models.Model):
    nama = models.CharField(max_length=50)
    kontak = models.CharField(max_length=50)
    laporan = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)
    #bukti = models.FileField(upload_to=None)

    #Sambungin Models milik apps laporan
    status = models.ForeignKey('laporan.PilihStatus', on_delete=models.DO_NOTHING, default=1)

    def __str__(self):
        return self.nama
    
    def status1(self):
        if (self.status.status == "Diproses"):
            return 'selected=""' 
    
    def status2(self):
        if (self.status.status == "Ditolak"):
            return 'selected=""'
    
    def status3(self):
        if (self.status.status == "Selesai"):
            return 'selected=""'