from django.db import models
from django.utils import timezone
from datetime import datetime, date

STATUS = [('Diproses', 'Diproses'),
          ('Ditolak', 'Ditolak'),
          ('Selesai', 'Selesai')]

class PilihStatus(models.Model):
    status = models.CharField(max_length=50, choices=STATUS, default="---")

    #Agar memunculkan "value" dari statusnya
    def __str__(self):
        return self.status
