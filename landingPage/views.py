from django.shortcuts import render, redirect
from landingPage.forms import UserForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm

# Create your views here.
def index(request):
    return render(request, 'landingPage/index.html')

def login_index(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            return redirect("/")
        
    return render(request, 'landingPage/login.html')

def register(request):
    if request.method == "POST":
        user_form = UserCreationForm(request.POST)

        if user_form.is_valid():
            user_form.save()
            return redirect("/")
    else:
        form = UserCreationForm()

        konten = {
            'form' : form,
        }
    return render(request, 'landingPage/register.html', konten)


def logout_view(request):
    logout(request)
    print(request.user)
    return redirect("/")