# mengikuti gitlab ppw 2017
 
from django.test import TestCase, Client
from django.urls import resolve
from .views import tentang_kami, jsonnya
from .models import FAQ
from .forms import FAQform

from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

class UnitTestAbout(TestCase):
    
    def setUp(self):
        self.client=Client()
        self.data = {'name':'dinda', 'email':'dinda.ip@gmail.com', 'question':'mau nanya dong'}
        self.wrongData = {'name':'dinda', 'email':'dinda.ip@gmail.com', 'question':''}

    def test_url_exist(self):
        resp = self.client.get('/tentang-kami/')
        self.assertEqual(resp.status_code, 200)

    def test_use_correct_func(self):
        found = resolve('/tentang-kami/')
        self.assertEqual(found.func, tentang_kami)

    def test_use_correct_template(self):
        resp = self.client.get('/tentang-kami/')
        self.assertTemplateUsed(resp, 'about/tentang_kami.html')

    def test_model_can_create_new_faq(self):
        new_faq = FAQ.objects.create(name='dinda', email='dinda.ip@gmail.com', question='mau nanya boleh ngga')
        counting_all_objects = FAQ.objects.all().count()
        
        self.assertEqual(counting_all_objects,1)

    def test_form_valid(self):
        # mengecek apakah jika isi form valid maka is_valid() true
        form = FAQform(self.data)
        self.assertTrue(form.is_valid())

    def test_form_validation_if_blank(self):
        form = FAQform(self.wrongData)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['question'], 
            ["This field is required."]
        )   

    def test_post_success_and_render_result(self):
        question='mau nanya dong'
        response_post = self.client.post('/tentang-kami/tambah-pertanyaan/', self.data)
        self.assertEqual(response_post.status_code, 302)

        resp = self.client.get('/tentang-kami/')
        html_resp = resp.content.decode('utf-8')
        self.assertIn(question, html_resp)

    def test_post_blank_and_render_result(self):
        question='mau nanya dong'
        response_post = self.client.post('/tentang-kami/tambah-pertanyaan/', self.wrongData)
        self.assertEqual(response_post.status_code, 302)

        resp = self.client.get('/tentang-kami/')
        html_resp = resp.content.decode('utf-8')
        self.assertNotIn(question, html_resp)

    def test_url_json_exist(self):
        resp = self.client.get('/tentang-kami/json/')
        self.assertEqual(resp.status_code, 200)

    # def test_json_correct_func(self):
    #     found = resolve('/tentang-kami/json')
    #     self.assertEqual(found.func, jsonnya)

class FuncTestAbout(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FuncTestAbout, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FuncTestAbout, self).tearDown()
    
    def test_show_questions(self):

        self.selenium.get('http://lapor-sis2.herokuapp.com/tentang-kami')
        time.sleep(3)
    
        nama = self.selenium.find_element_by_name('name')
        surel = self.selenium.find_element_by_name('email')
        tanya = self.selenium.find_element_by_name('question')

        nama.send_keys('dinda')
        surel.send_keys('dinda.ip@gmail.com')
        tanya.send_keys('mau nanya dong')

        submit = self.selenium.find_element_by_id('submit_btn')
        submit.click()

        time.sleep(5)
        
        self.assertIn("dinda", self.selenium.page_source)
        self.assertIn("mau nanya dong", self.selenium.page_source)

    def test_hover(self):

        self.selenium.get('http://lapor-sis2.herokuapp.com/tentang-kami')
        time.sleep(3)
    
        sis = self.selenium.find_element_by_id('sis')
        hover = ActionChains(self.selenium).move_to_element(sis)
        hover.perform()
        warna = sis.value_of_css_property('color')

        self.assertTrue(warna, "rgba(242, 133, 138, 1)")

    def test_popup(self):

        self.selenium.get('http://lapor-sis2.herokuapp.com/tentang-kami')
        time.sleep(3)
    
        img = self.selenium.find_element_by_id('alur')
        img.click();       

        pop = self.selenium.find_element_by_class_name('pop_up')
        display = pop.value_of_css_property('display') 

        self.assertTrue(display, "block")