from django.db import models

class FAQ(models.Model):
    name = models.CharField(max_length=50)
    email = models.EmailField()
    question = models.TextField()

    def __str__(self):
        return self.name
