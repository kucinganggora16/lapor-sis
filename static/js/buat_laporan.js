$("document").ready(function() {
    async function getData() {
        const request = await fetch("/api/")
        const rjson = await request.json()
        console.log(rjson)
        const s = rjson.map(function(a) {
            return `
            <tr>
            <td>${a.nama}</td>
            </tr>
            `;
        })
        $("#table-body").html(s.join(""))
    }

    getData()

    $("#laporan-form").submit(async function(e) {
        e.preventDefault();

        const POSTURL = "/api/create/"

        const nama = $("#id_nama").val();
        const kontak = $("#id_kontak").val();
        const laporan = $("#id_laporan").val();

        const csrfToken = $("input[name='csrfmiddlewaretoken']").val();

        const data = {
            nama,
            kontak,
            laporan
        };

        const reqOptions = {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': csrfToken
            },
            body: JSON.stringify(data)
        };

        const postRequest = await fetch(POSTURL, reqOptions)
        const postResult = await postRequest.json()
        $("#laporan-form")[0].reset()
        console.log("Success!", postResult)
        $("#alert-success").show()
        getData()
    })


    let lightTheme = true;

    $("#ubah").click(function() {
        lightTheme = !lightTheme;
        if (lightTheme === true) {
            $("body").css("background-color", "#e8f0f4")
            $("body").css("transition", "1s")
            $("body").css("color", "black")
        }
        else {
            $("body").css("background-color", "#404040")
            $("body").css("transition", "1s")
            $("body").css("color", "#e8f0f4")
        }
    });

    $("#submit-button").hover(function(){
        $(this).css("background-color", "red");
        },
        function(){
        $(this).css("background-color", "#f2858a");
    });

    $('#id_laporan').keydown(async function (e) {
        if (e.keyCode == 13) {
            if (e.altKey) {                
                e.preventDefault();

                const POSTURL = "/api/create/"

                const nama = $("#id_nama").val();
                const kontak = $("#id_kontak").val();
                const laporan = $("#id_laporan").val();

                const csrfToken = $("input[name='csrfmiddlewaretoken']").val();

                const data = {
                    nama,
                    kontak,
                    laporan
                };

                const reqOptions = {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json',
                        'X-CSRFToken': csrfToken
                    },
                    body: JSON.stringify(data)
                };

                const postRequest = await fetch(POSTURL, reqOptions)
                const postResult = await postRequest.json()
                $("#laporan-form")[0].reset()
                console.log("Success!", postResult)
                $("#alert-success").show()
                getData()
            }
        }
    });


});