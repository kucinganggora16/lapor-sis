from django.test import TestCase, Client, LiveServerTestCase
from django.urls import reverse, resolve
from .models import Laporan
from .views import buatLaporan
from laporan.models import PilihStatus
from django.contrib.auth import get_user_model
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import chromedriver_binary

# Create your tests here.
class FormTest(TestCase):

    def setUp(self):
        User = get_user_model()
        user = User.objects.create_user('temporary', 'temporary@gmail.com', 'temporary')
        self.client.login(username='temporary', password='temporary')

    def test_buatLaporan_url_exist(self):
        response = self.client.get("/buatLaporan/")
        self.assertEqual(response.status_code, 200)
    
    def test_using_right_template(self):
        response = self.client.get(reverse('formLaporan:buatLaporan'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'formLaporan/buat_laporan.html')

    def test_page_using_func(self):
        found = resolve('/buatLaporan/')
        self.assertEqual(found.func, buatLaporan)
    
    def test_form_exist(self):
        response = self.client.get('/buatLaporan/')
        isi_html = response.content.decode("utf8")
        self.assertIn("<form", isi_html)
    
    def test_button_laporsis_exist(self):
        response = self.client.get('/buatLaporan/')
        isi_html = response.content.decode("utf8")
        self.assertIn("<input", isi_html)
        self.assertIn('type="submit"', isi_html)
        self.assertIn("Lapor-sis!", isi_html)
    
    def test_post_and_saved(self):
        self.client.post('/buatLaporan/', {'nama':'x', 'kontak':'123', 'laporan': 'test'})
        PilihStatus.objects.create(status = "DITOLAK")
        self.assertEqual(True, Laporan.objects.filter(nama='x').exists())
        self.assertEqual(True, Laporan.objects.filter(kontak='123').exists())
        self.assertEqual(True, Laporan.objects.filter(laporan='test').exists())
    
    def test_model(self):
        Laporan.objects.create(laporan = "test juga")
        PilihStatus.objects.create(status = "DITOLAK")
        count = Laporan.objects.all().count()
        self.assertEqual(1, count)