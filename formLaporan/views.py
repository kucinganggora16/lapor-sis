from django.shortcuts import render
from .forms import BuatLaporan
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from landingPage.views import login

# Create your views here.
@login_required
def buatLaporan(request):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = BuatLaporan(request.POST or None)
        # check whether it's valid:
        if form.is_valid():
            form.save()
            form = BuatLaporan()
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            return HttpResponseRedirect('/buatLaporan/')
    
    # if a GET (or any other method) we'll create a blank form
    else:
        form = BuatLaporan()


    return render(request, 'formLaporan/buat_laporan.html', {'form':form})